## 0.0.3
 - Add some documentation

## 0.0.2
 - Fix packaging (utils package not being included previously).

## 0.0.1
 - Initial release on PyPi.